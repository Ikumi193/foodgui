import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class FoodGUI {

    private JPanel root;
    private JButton ramenButton;
    private JButton maboButton;
    private JButton curryButton;
    private JTextPane orderList;
    private JButton hinabeButton;
    private JButton pizzaButton;
    private JButton pastaButton;
    private JButton checkoutButton;
    private JLabel total;
    private JLabel firstmessage;
    private JTextPane priceList;
    private JTextPane spicyList;
    private JLabel quantity;
    int price,sokei,spicy,i=0;

     void order(String food,int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

         int spiceconfirmation=2;
         if (confirmation == 0) {
            spiceconfirmation = JOptionPane.showConfirmDialog(
                    null,
                    "Would you like to add more spice? ",
                    "Spicy Option",
                    JOptionPane.YES_NO_OPTION);
            JOptionPane.showMessageDialog(null, "Order for "+food+" received");
            orderList.setText(orderList.getText() +food+ "\n");
            priceList.setText(priceList.getText() +price+ " Yen\n");
            sokei += price;
            i++;
            total.setText(+sokei+" Yen\n");
            quantity.setText(+i+"\n");
        }

        if(spiceconfirmation == 0){
            spicyList.setText(spicyList.getText() +"Yes\n");
        }
        else if(spiceconfirmation == 1){
            spicyList.setText( spicyList.getText()+"No\n");
        }
    }


    public FoodGUI() {
        ramenButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                order("Ramen",1200);
            }
        });

        maboButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Mabodofu",1000);

            }

        });

        curryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Curry",1300);
            }
        });

        hinabeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                order("Hinabe",1800);
            }
        });


        pizzaButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                order("Pizza",1500);
            }
        });


        pastaButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                order("Pasta",1400);
            }
        });


        checkoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0){
                    JOptionPane.showMessageDialog(null,"Thank you. The total price is " + sokei + " yen.");
                    sokei = 0;
                    i=0;
                    total.setText("total:"+sokei+"\n");
                    orderList.setText("");
                    spicyList.setText("");
                    priceList.setText("");
                    total.setText("");
                    quantity.setText("");

                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}


